CC = gcc
CFLAGS = -std=c99 -g -O -Wall -Wextra -Wpedantic

program = tic-tac-toe
objects = tic_tac_toe.o main.o

test_program = ttt-test
test_objects = tic_tac_toe.o tic_tac_toe_test.o

all: $(program)

$(program): $(objects)
	$(CC) $(CFLAGS) -o $@ $^

tests: $(test_program)

$(test_program): $(test_objects)
	$(CC) $(CFLAGS) -o $@ $^

clean:
	$(RM) $(test_program) $(test_objects) $(program) $(objects)

.PHONY: all tests clean
