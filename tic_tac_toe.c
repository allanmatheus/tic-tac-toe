#include "tic_tac_toe.h"

#include <stdbool.h>
#include <stddef.h>

int ttt_init(struct tic_tac_toe *self)
{
	if (NULL == self) {
		return -1;
	}
	self->current_player = player_a;
	for (int i = 0; i < board_max_rows; i++) {
		for (int j = 0; j < board_max_cols; j++) {
			self->board[i][j] = no_player;
		}
	}
	return 0;
}

static bool is_row_complete(const struct tic_tac_toe *game, int row)
{
	return no_player != game->board[row][0]
	    && game->board[row][0] == game->board[row][1]
	    && game->board[row][0] == game->board[row][2];
}

static bool is_col_complete(const struct tic_tac_toe *game, int col)
{
	return no_player != game->board[0][col]
	    && game->board[0][col] == game->board[1][col]
	    && game->board[0][col] == game->board[2][col];
}

static bool is_diag_complete(const struct tic_tac_toe *game)
{
	bool primary_diag = no_player != game->board[0][0]
	    && game->board[0][0] == game->board[1][1]
	    && game->board[0][0] == game->board[2][2];
	bool secondary_diag = no_player != game->board[0][2]
	    && game->board[0][2] == game->board[1][1]
	    && game->board[0][2] == game->board[2][0];
	return secondary_diag || primary_diag;
}

static bool game_over(const struct tic_tac_toe *game, enum ttt_player *winner)
{
	if (NULL == game) {
		return true;
	}
	for (int i = 0; i < board_max_rows; i++) {
		if (is_row_complete(game, i)) {
			if (NULL != winner) {
				*winner = game->board[i][0];
			}
			return true;
		}
	}
	for (int i = 0; i < board_max_cols; i++) {
		if (is_col_complete(game, i)) {
			if (NULL != winner) {
				*winner = game->board[0][i];
			}
			return true;
		}
	}
	if (is_diag_complete(game)) {
		if (NULL != winner) {
			*winner = game->board[1][1];
		}
		return true;
	}
	return false;
}

bool ttt_is_game_over(const struct tic_tac_toe *self)
{
	return game_over(self, NULL);
}

static void swap_players(struct tic_tac_toe *game)
{
	if (NULL == game) {
		return;
	}
	if (player_a == game->current_player) {
		game->current_player = player_b;
	} else if (player_b == game->current_player) {
		game->current_player = player_a;
	}
}

int ttt_game_round(struct tic_tac_toe *self, int row, int col)
{
	if (ttt_is_game_over(self)) {
		return -1;
	}
	bool valid_row = 0 <= row && row < board_max_rows;
	bool valid_col = 0 <= col && col < board_max_cols;
	if (!valid_row || !valid_col) {
		return -1;
	}
	if (NULL == self) {
		return -1;
	}
	char old_value = self->board[row][col];
	if (no_player != old_value) {
		return -1;
	}
	self->board[row][col] = self->current_player;
	swap_players(self);
	return 0;
}

enum ttt_player ttt_winner(const struct tic_tac_toe *self)
{
	enum ttt_player winner = no_player;
	if (NULL == self) {
		return no_player;
	}
	if (game_over(self, &winner)) {
		return winner;
	}
	return no_player;
}
