#include "tic_tac_toe.h"

#include <stdio.h>
#include <stdlib.h>

static void print_board(const struct tic_tac_toe *game)
{
	for (int i = 0; i < board_max_rows; i++) {
		printf("   |   |\n");
		for (int j = 0; j < board_max_cols; j++) {
			printf(" %c", game->board[i][j]);
			if (j < 2) {
				printf(" |");
			}
		}
		printf("\n");
		if (i < 2) {
			printf("___|___|___\n");
		}
	}
}

static void ask_user(struct tic_tac_toe *game)
{
	int row, col, err;
	do {
		printf("[Player %c] row: ", game->current_player); scanf(" %d", &row);
		printf("[Player %c] col: ", game->current_player); scanf(" %d", &col);
		err = ttt_game_round(game, row, col);
	} while (err);
}

static void main_loop(struct tic_tac_toe *game)
{
	printf("===============\n");
	printf("  TIC-TAC-TOE\n");
	printf("===============\n");
	while (!ttt_is_game_over(game)) {
		print_board(game);
		ask_user(game);
		system("clear");
	}
	printf("===============\n");
	printf("Player %c Wins!\n", ttt_winner(game));
	printf("===============\n");
	print_board(game);
	printf("===============\n");
}

static void demo(void)
{
	struct tic_tac_toe game;
	ttt_init(&game);
	ttt_game_round(&game, 0, 0);
	ttt_game_round(&game, 0, 1);
	ttt_game_round(&game, 1, 1);
	ttt_game_round(&game, 2, 0);
	ttt_game_round(&game, 2, 2);
	print_board(&game);
}

int main(int argc, char **argv)
{
	(void) argc;
	(void) argv;
	struct tic_tac_toe game;
	ttt_init(&game);
	demo();
	main_loop(&game);
	return 0;
}
