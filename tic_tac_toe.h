#ifndef TIC_TAC_TOE_H
#define TIC_TAC_TOE_H

#include <stdbool.h>

enum {
	board_max_rows = 3,
	board_max_cols = 3,
};

enum ttt_player {
	player_a = 'X',
	player_b = 'O',
	no_player = ' '
};

struct tic_tac_toe {
	char board[board_max_rows][board_max_cols];
	enum ttt_player current_player;
};

/**
 * Initializes the game.
 */
int ttt_init(struct tic_tac_toe *self);

/**
 * Verify if the game is over.
 */
bool ttt_is_game_over(const struct tic_tac_toe *self);

/**
 * Saves the player's round at the board.
 */
int ttt_game_round(struct tic_tac_toe *self, int row, int col);

/**
 * Returns the winner of the game if the game is over. Otherwise it returns
 * `invalid_player`.
 */
enum ttt_player ttt_winner(const struct tic_tac_toe *self);

#endif				/* TIC_TAC_TOE_H */
