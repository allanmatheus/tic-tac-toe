# Tic-Tac-Toe

A TDD implementation in C of the Tic-Tac-Toe game with.

## Build & Run

```
$ make
$ ./tic-tac-toe
```

## Tests

```
$ make tests
$ ./ttt-test
```

The ```ttt-test``` will alert you when an error occur. If you ran it and saw
nothing, then you're ready to play!
