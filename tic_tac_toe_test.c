#include "tic_tac_toe.h"

#include <assert.h>
#include <stddef.h>
#include <stdbool.h>

static void test_invalid_game(void)
{
	assert(ttt_init(NULL) == -1 && "test_invalid_game() unsuccessful init");
	assert(ttt_is_game_over(NULL) == true
	       && "test_invalid_game() quick game over");
	assert(ttt_game_round(NULL, 0, 0) == -1
	       && "test_invalid_game() unsuccessful round");
	assert(ttt_winner(NULL) == no_player
	       && "test_invalid_game() no winner");
}

static void test_game_base(void)
{
	struct tic_tac_toe game;
	assert(ttt_init(&game) == 0 && "test_game_base() successful init");
	assert(!ttt_is_game_over(&game)
	       && "test_game_base() initial game not over");
}

static void test_game_init(void)
{
	struct tic_tac_toe game;
	ttt_init(&game);
	bool valid_player = player_a == game.current_player
	    || player_b == game.current_player;
	assert(valid_player && "test_game_init() player init");
	for (int i = 0; i < board_max_rows; i++) {
		for (int j = 0; j < board_max_cols; j++) {
			assert(no_player == game.board[i][j]
			       && "test_game_init() board init");
		}
	}
}

static void test_move(void)
{
	struct tic_tac_toe game;
	ttt_init(&game);
	assert(ttt_game_round(&game, 0, 0) == 0 && "test_move() good move");
	assert(ttt_game_round(&game, 0, 0) == -1
	       && "test_move() bad move: already used");
	assert(ttt_game_round(&game, 3, 1) == -1
	       && "test_move() bad move: out of range");
	assert(ttt_game_round(&game, 1, 3) == -1
	       && "test_move() bad move: out of range");
}

static void test_player_swap(void)
{
	struct tic_tac_toe game;
	ttt_init(&game);
	enum ttt_player current_player = game.current_player;
	ttt_game_round(&game, 0, 0);
	assert(current_player != game.current_player
	       && "test_player_swap() swap");
	ttt_game_round(&game, 1, 0);
	assert(current_player == game.current_player
	       && "test_player_swap() swap back");
}

static void test_game_over_horizontal(void)
{
	struct tic_tac_toe game;
	ttt_init(&game);
	ttt_game_round(&game, 0, 0);	// player_a
	ttt_game_round(&game, 1, 1);	// player_b
	ttt_game_round(&game, 0, 1);	// player_a
	ttt_game_round(&game, 2, 2);	// player_b
	ttt_game_round(&game, 0, 2);	// player_a
	assert(ttt_is_game_over(&game) == true
	       && "test_game_over_horizontal()");
}

static void test_game_over_vertical(void)
{
	struct tic_tac_toe game;
	ttt_init(&game);
	ttt_game_round(&game, 0, 0);	// player_a
	ttt_game_round(&game, 1, 1);	// player_b
	ttt_game_round(&game, 1, 0);	// player_a
	ttt_game_round(&game, 2, 2);	// player_b
	ttt_game_round(&game, 2, 0);	// player_a
	assert(ttt_is_game_over(&game) == true && "test_game_over_vertical()");
}

static void test_game_over_diagonal(void)
{
	struct tic_tac_toe game;
	ttt_init(&game);
	ttt_game_round(&game, 0, 0);	// player_a
	ttt_game_round(&game, 1, 0);	// player_b
	ttt_game_round(&game, 1, 1);	// player_a
	ttt_game_round(&game, 2, 0);	// player_b
	ttt_game_round(&game, 2, 2);	// player_a
	assert(ttt_is_game_over(&game) == true
	       && "test_game_over_diagonal() primary");
	ttt_init(&game);
	ttt_game_round(&game, 0, 2);	// player_a
	ttt_game_round(&game, 1, 0);	// player_b
	ttt_game_round(&game, 1, 1);	// player_a
	ttt_game_round(&game, 1, 2);	// player_b
	ttt_game_round(&game, 2, 0);	// player_a
	assert(ttt_is_game_over(&game) == true
	       && "test_game_over_diagonal() secondary");
}

static void test_game_over(void)
{
	test_game_over_horizontal();
	test_game_over_vertical();
	test_game_over_diagonal();
}

static void test_round_after_game_over(void)
{
	struct tic_tac_toe game;
	ttt_init(&game);
	assert(ttt_game_round(&game, 0, 0) == 0
	       && "test_round_after_game_over() before game over");
	assert(ttt_game_round(&game, 1, 0) == 0
	       && "test_round_after_game_over() before game over");
	assert(ttt_game_round(&game, 1, 1) == 0
	       && "test_round_after_game_over() before game over");
	assert(ttt_game_round(&game, 2, 0) == 0
	       && "test_round_after_game_over() before game over");
	assert(ttt_game_round(&game, 2, 2) == 0
	       && "test_round_after_game_over() game over");
	assert(ttt_game_round(&game, 0, 1) == -1
	       && "test_round_after_game_over() after game over");
}

static void test_who_won(void)
{
	struct tic_tac_toe game;
	ttt_init(&game);
	ttt_game_round(&game, 0, 0);	// player_a
	ttt_game_round(&game, 1, 0);	// player_b
	ttt_game_round(&game, 1, 1);	// player_a
	ttt_game_round(&game, 2, 0);	// player_b
	ttt_game_round(&game, 2, 2);	// player_a
	assert(ttt_winner(&game) == player_a && "test_who_won() player_a");
	ttt_init(&game);
	ttt_game_round(&game, 0, 0);	// player_a
	ttt_game_round(&game, 0, 1);	// player_b
	ttt_game_round(&game, 1, 0);	// player_a
	ttt_game_round(&game, 1, 1);	// player_b
	ttt_game_round(&game, 2, 2);	// player_a
	ttt_game_round(&game, 2, 1);	// player_b
	assert(ttt_winner(&game) == player_b && "test_who_won() player_b");
}

int main(void)
{
	test_game_base();
	test_invalid_game();
	test_game_init();
	test_move();
	test_player_swap();
	test_game_over();
	test_round_after_game_over();
	test_who_won();
	return 0;
}
